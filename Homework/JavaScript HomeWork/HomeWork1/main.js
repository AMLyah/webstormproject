// При обьявлении переменной через var-она работает по всему коду,
//     через let-в пределах конкретного блока.
//     Const-работает по принципу let но априори остается константой
//  Использование var считается плохим тоном, потому что если использовать переменную с именем, которое упомяналось в коде ранее, то ее значение будет браться с верхней части кода.
let name = prompt("Enter your name");
let age = prompt("Enter your age");
if (age < 18){
    alert("You are not allowed to visit this website")
}
else if (age >= 18 && age <= 22 ) {
    if(confirm("Are you sure you want to continue?")){
     alert("Welcome " + name )
    }else
        alert("You are not allowed to visit this website")
}
else if(age > 22){
    alert("Welcome " + name )
}
